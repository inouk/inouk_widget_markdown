{
    "name": "Inouk Widget Markdown",
    "summary": """
        Allow to enter Markdown in Text fields using SimpleMDE (https://simplemde.com/)
    """,
    "author": "Cyril MORISSE",
    "website": "https://gitlab.com/inouk/inouk_widget_markdown",
    "category": "web",
    "version": "16.0.1.0.0",
    "license": "LGPL-3",
    "depends": ["base", "web"],
    "data": [],
    "qweb": [],
    "assets": {
        "web.assets_backend": [
            "/inouk_widget_markdown/static/lib/simplemde.min.css",

            "/inouk_widget_markdown/static/src/js/markdown_field.js",
            "/inouk_widget_markdown/static/src/xml/markdown_field.xml",
            "/inouk_widget_markdown/static/src/css/inouk_markdown.css",

        ]
    },
    "auto_install": False,
    "installable": True,
}
