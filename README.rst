Inouk Widget Markdown 
=====================

A widget that allows users to enter 'rendered' Markdown in Text fields.

This module is based on `SimpleMDE <https://simplemde.com/>`__

Usage in backend views
----------------------

Add ``widget="inouk_markdown"`` to any Text field in your xml view.

The options dict is passed to SimpleMDE as `Configuration` options.

.. code:: xml

       <field name="my_description" widget="inouk_markdown" options="{'spellChecker': false}">

Usage in Report and Website
---------------------------

This widget cannot be used on website or reports for now.

Bugs
----

Please report bugs to https://gitlab.com/inouk/inouk_widget_markdown

Author(s)
---------

- Cyril MORISSE

Credits
---------

Thanks to https://codingdodo.com/ for their excellent OWL tutorials.
