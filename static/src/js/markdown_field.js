/** @odoo-module **/
import {useRef, useEffect, onWillStart, markup, Component, onWillUpdateProps} from "@odoo/owl";
import {loadJS} from "@web/core/assets";
import {registry} from "@web/core/registry";
import {useBus} from "@web/core/utils/hooks";
import {standardFieldProps} from "@web/views/fields/standard_field_props";
import {TranslationButton} from "@web/views/fields/translation_button";
import {debounce} from "@web/core/utils/timing";

export class MarkdownField extends Component {
    setup() {
        super.setup();
        this.simpleMDEditor = null;
        this.editorRef = useRef("textarea");

        onWillStart(async () => {
            await loadJS("/inouk_widget_markdown/static/lib/simplemde.min.js");
        });

        onWillUpdateProps(this.updateSimpleMDE);

        useEffect(
            () => {
                this.setupSimpleMDE();
                this.updateSimpleMDE(this.props);
                return () => this.destroySimpleMDE();
            },
            () => [this.editorRef.el]
        );

        useBus(this.env.bus, "RELATIONAL_MODEL:WILL_SAVE_URGENTLY", () => this.commitChanges());
        useBus(this.env.bus, "RELATIONAL_MODEL:NEED_LOCAL_CHANGES", ({ detail }) =>
            detail.proms.push(this.commitChanges())
        );
    }

    /**
     * Instanciate the editor
     * retrieve the options passed to the XML field definition.
     */
    setupSimpleMDE() {
        if(this.props.readonly) return;
        var simplemdeConfig = {
            element: this.editorRef.el,
            initialValue: '',
            forceSync: true
        };
        if (this.props.editorOptions) {
            simplemdeConfig = {...simplemdeConfig, ...this.props.editorOptions};
        }
        this.simpleMDEditor = new SimpleMDE(simplemdeConfig);
        this.simpleMDEditor.codemirror.on("blur", this.commitChanges.bind(this));
        this.simpleMDEditor.codemirror.on("change", debounce(this.commitChanges.bind(this), 500));
    }

    updateSimpleMDE({ readonly, value }) {
        if (!this.simpleMDEditor) {
            return;
        }
        
        /* TODO: Find a way to switch in readonly.
        this.aceEditor.setOptions({
            readOnly: readonly
        });
        this.aceEditor.renderer.$cursorLayer.element.style.display = readonly ? "none" : "block";
        */

        if (this.simpleMDEditor.value() !== value) {
            this.simpleMDEditor.value(value);
        }
    }

    destroySimpleMDE() {
        if (this.simpleMDEditor) {
            this.simpleMDEditor.toTextArea();
            this.simpleMDEditor = null;
        }
    }

    commitChanges() {
        if (!this.props.readonly) {
            const value = this.simpleMDEditor.value();
            if (this.props.value !== value) {
                return this.props.update(value);
            }
        }
    }

    get HtmlValue() {
        return markup(SimpleMDE.prototype.markdown(this.props.value));
    }

    
}


MarkdownField.template = "inouk_widget_markdown.MarkdownField";
MarkdownField.props = {
    ...standardFieldProps,
    isTranslatable: {type: Boolean, optional: true},
    fieldName: {type: String, optional: true},
    editorOptions: {type: Object, optional: true},
};
MarkdownField.extractProps = ({attrs, field}) => {
    return {
        isTranslatable: field.translate,
        fieldName: field.name,
        editorOptions: {
            ...attrs.options,
            placeholder: attrs.placeholder,
        },
    };
};
MarkdownField.components = {
    TranslationButton,
};

registry.category("fields").add("inouk_markdown", MarkdownField);
